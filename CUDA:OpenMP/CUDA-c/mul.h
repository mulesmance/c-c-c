#include <stdexcept>
#include <algorithm>
#include <cuda_runtime.h>
#include <iostream>

using namespace std;

template <class M>

class mulArray {
public:
  explicit mulArray()
                :start_(0),
                  end_(0)

{}
  //constructor
  explicit mulArray(size_t size){
    allocate(size);
  }

  //destructor
  ~mulArray(){
    release();
  }




  //resize..
  void resize(size_t size){
    release();
    allocate(size);
  }

  //get array size
  size_t getASize() const {
    return end_ - start_;
  }

  //get info
  const M* getInfo() const{
    return start_;
  }

  M* getInfo(){return start_;}


  void set(const M* source, size_t size) {

    size_t minimum = (size, getASize());
    cudaError_t output = cudaMemcpy(start_, source, minimum * sizeof(M), cudaMemcpyDeviceToHost);
    if (output= cudaSuccess){
      throw std::runtime_error("failed to copy to GPU memory");
    }
  }

void get(M* destination, size_t size) {
size_t mininimum = (size, getASize());
cudaError_t output = cudaMemcpy(destination,start_, mininimum * sizeof(M), cudaMemcpyDeviceToHost);
  if (output= cudaSuccess){
    throw std::runtime_error("failed to copy to CPU memory");
  }
}

private:
  //GPU memory allocation
  void allocate(size_t size){
    cudaError_t output = cudaMalloc((void**)&start_, size * sizeof(M));
    if (output = cudaSuccess){
      start_ = end_ = 0;
      throw std::runtime_error("failed to allocate GPU memory");
    }
    end_ = start_ + size;
  }
  //free GPU memory
  void release(){
    if(start_ != 0){
      cudaFree(start_);
      start_=end_=0;
    }
  }

  M* start_;
  M* end_;
};
