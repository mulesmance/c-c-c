#include <iostream>
#include <cstdio>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <cuda_runtime.h>
#include "mul.h"

#include <math.h>
using namespace std;
// Kernel function to add the elements of two arrays
__global__ void matrixMul(float *a, float *b, float *c, int n)
{
    int row = threadIdx.y* + blockIdx.y*blockDim.y;
    int col = threadIdx.x* + blockIdx.x*blockDim.x;

    float tempSum = 0;

    if(row < n && col < n){

      for (int i=0; i < n; i++){
        tempSum += a[row * n + i] * b[i * n + col];
      }
    }
    c[row * n + col] = tempSum;
}

void matrixMulti(float *a, float *b, float *c, int n){
  dim3 threadsPerBlock(n,n);
  dim3 blocksPerGrid(1,1);


    matrixMul <<<blocksPerGrid, threadsPerBlock>>>(a, b, c, n);
  }



int main(void)
{
  int n = 14;
  int size = n*n;

  // Allocate Memory  on GPU
  vector<float> G_a(size);
  vector<float> G_b(size);
  vector<float> G_c(size);

  //initialize metrices on GPU
  for(int i = 0; i < n; i++){
    for (int j=0; j<n; j++){
        G_a[n*i+j] = sin(i);
        G_b[n*i+j] = cos(j);
    }
  }

  // Allocate Memory on CPU
  mulArray<float> C_a(size);
  mulArray<float> C_b(size);
  mulArray<float> C_c(size);

  cudaEvent_t start, end;
  cudaEventCreate  (&start);
  cudaEventCreate  (&end);
  // initialize arrays on the CPU
  C_a.set(&G_a[0], size);
  C_b.set(&G_b[0], size);

  matrixMulti(C_a.getInfo(), C_b.getInfo(), C_c.getInfo(), n);
  cudaDeviceSynchronize();

  C_c.get(&G_c[0], size);
  cudaDeviceSynchronize();

  float *CPU_c;
  CPU_c=new float[size];

  //perform matrix multiplicaion on CPU
  float output;
  for(int row=0; row<n; row++){
    for(int col=0; col<n; col++){
      output = 0.f;
      for(int nn=0; nn<n; nn++ ){
        output += G_a[n*row+nn]*G_b[nn*n+col];
      }
      CPU_c[row*n+col] = output;
    }
}
void inputDev();
//void inputHost();
//void stream();
//void MyKernel();
//void outputDev();
//void outputHost();

cudaEventRecord(start, 0);
for(int i = 0; i < 2; ++i) {
  //cudaMemcpyAsync(inputDev + i * size, inputHost + i * size, size, cudaMemcpyHostToDevice, stream[i]);
  //MyKernel <<<100, 512, 0, stream[i]>>>
    //      (outputDev + i * size, inputDev + i * size, size);
  //cudaMemcpyAsync(outputHost + i * size, outputDev + i * size, size, cudaMemcpyDeviceToHost, stream[i]);
}
cudaEventRecord(end, 0);
cudaEventSynchronize(end);
float elapsedTime;
cudaEventElapsedTime(&elapsedTime, start, end);
printf("Elapsed time = %f\n ", elapsedTime);

cudaEventDestroy(start);
cudaEventDestroy(end);


  return 0;
}
