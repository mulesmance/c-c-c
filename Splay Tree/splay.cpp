/* 
 * C++ Project to implement Splay treee
 * Program by: Tunbosun (Michael) Olaniyi & Najib Saliu-Ahmed
 * Data Structure And Algorithm
 *****************************************************************/
#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;
struct splay
{
       int key;
       splay* leftChild;
       splay* rightChild;
};
class SplayTree
{
public:
   SplayTree(){}
// RR(Y rotation to the right)
   splay* RR_Rotate(splay* k2)
   {
      splay* k1 = k2->leftChild;
      k2->leftChild = k1->rightChild;
      k1->rightChild = k2;
      return k1;
   }
   // LL(Y rotatation to the left)
   splay* LL_Rotate(splay* k2)
   {
      splay* k1 = k2->rightChild;
      k2->rightChild = k1->leftChild;
      k1->leftChild = k2;
      return k1;
   }
// An implementation of top-down splay tree
   splay* Splay(int key, splay* root)
   {
      if (!root)
	 return NULL;
      splay header;
      /* header.rightChild points to Left side of tree & header.leftChild points to Right side of tree */
      header.leftChild = header.rightChild = NULL;
      splay* LeftTreeMax = &header;
      splay* RightTreeMin = &header;
      while (1)
      {
              if (key < root->key)
	      {
		 if (!root->leftChild)
		    break;
		 if (key < root->leftChild->key)
		 {
		    root = RR_Rotate(root);
		    //zig zag need rotation only once
		    if (!root->leftChild)
		       break;
		 }
		 /* Link to right tree */
		 RightTreeMin->leftChild = root;
		 RightTreeMin = RightTreeMin->leftChild;
		 root = root->leftChild;
		 RightTreeMin->leftChild = NULL;
	      }
	      else if (key > root->key)
	      {
		 if (!root->rightChild)
		    break;
		 if (key > root->rightChild->key)
		 {
		    root = LL_Rotate(root);
		    // only zag-zag mode need to rotate once,
		    if (!root->rightChild)
		       break;
		 }
		 /* Link to left tree */
		 LeftTreeMax->rightChild = root;
		 LeftTreeMax = LeftTreeMax->rightChild;
		 root = root->rightChild;
		 LeftTreeMax->rightChild = NULL;
	      }
	      else
		 break;
      }
      /* assemble the left tree, middle tree and the right tree */
      LeftTreeMax->rightChild = root->leftChild;
      RightTreeMin->leftChild = root->rightChild;
      root->leftChild = header.rightChild;
      root->rightChild = header.leftChild;
      return root;
   }

   splay* New_Node(int key)
   {
      splay* p_node = new splay;
      if (!p_node)
      {
	 fprintf(stderr, "Out of memory!\n");
	 exit(1);
      }
      p_node->key = key;
      p_node->leftChild = p_node->rightChild = NULL;
      return p_node;
   }
   
   splay* Insert(int key, splay* root)
   {
      static splay* p_node = NULL;
      if (!p_node)
	 p_node = New_Node(key);
      else
	 p_node->key = key;
      if (!root)
      {
	 root = p_node;
	 p_node = NULL;
	 return root;
      }
      root = Splay(key, root);
      /* This is BST that, all keys <= root->key is in root->lchild, all keys >
	 root->key is in root->rchild. */
      if (key < root->key)
      {
	 p_node->leftChild = root->leftChild;
	 p_node->rightChild = root;
	 root->leftChild = NULL;
	 root = p_node;
      }
      else if (key > root->key)
      {
	 p_node->rightChild = root->rightChild;
	 p_node->leftChild = root;
	 root->rightChild = NULL;
	 root = p_node;
      }
      else
	 return root;
      p_node = NULL;
      return root;
   }
   
   splay* Delete(int key, splay* root)
   {
      splay* temp;
      if (!root)
	 return NULL;
      root = Splay(key, root);
      if (key != root->key)
	 return root;
      else
      {
	 if (!root->leftChild)
	 {
	    temp = root;
	    root = root->rightChild;
	 }
	 else
	 {
	    temp = root;
	    /*Note: Since key == root->key,
	      so after Splay(key, root->lchild),
	      the tree we get will have no right child tree.*/
	    root = Splay(key, root->leftChild);
	    root->rightChild = temp->rightChild;
	 }
	 free(temp);
	 return root;
      }
   }
   
   splay* Search(int key, splay* root)
   {
      return Splay(key, root);
   }
   //this method makes sure that the structural property is being obeyed
   void InOrder(splay* root)
   {
      if (root)
      {
	 InOrder(root->leftChild);
	 cout<< "key: " <<root->key;
	 if(root->leftChild)
	    cout<< " | left child: "<< root->leftChild->key;
	 if(root->rightChild)
	    cout << " | right child: " << root->rightChild->key;
	 cout<< "\n";
	 InOrder(root->rightChild);
      }
   }
};


int main()
{
   SplayTree st;
   int vector[1000000] = {};
   splay *root;
   root = NULL;
   const int length = 10;
   int i;
   for(i = 0; i < length; i++)
      root = st.Insert(vector[i], root);
   cout<<"\nInOrder: \n";
   st.InOrder(root);
   int input, choice;
   while(1)
   {
      cout<<"\nSplay Tree Operations\n";
      cout<<"1. Insert node "<<endl;
      cout<<"2. Delete node"<<endl;
      cout<<"3. Find  node"<<endl;
      cout<<"4. Quit"<<endl;
      cout<<"Enter your choice: ";
      cin>>choice;
      switch(choice)
      {
	 case 1:
	    cout<<"Enter value to be inserted: ";
	    cin>>input;
	    root = st.Insert(input, root);
	    cout<<"\nAfter Insert: "<<input<<endl;
	    st.InOrder(root);
	    break;
	 case 2:
	    cout<<"Enter value to be deleted: ";
	    cin>>input;
	    root = st.Delete(input, root);
	    cout<<"\nAfter Delete: "<<input<<endl;
	    st.InOrder(root);
	    break;
	 case 3:
	    cout<<"Enter value to look for: ";
	    cin>>input;
	    root = st.Search(input, root);
	    cout<<"\nAfter Search "<<input<<endl;
	    st.InOrder(root);
	    break;
	 case 4:
	    exit(1);
	 default:
	    cout<<"\nInvalid type! \n";
      }
   }
   return 0;
}
