**************************************************************************************************************
Welcome!

In order to run the game, please open the Group_Project.cbp file, from which the program can be built and run.
The games available within the program are 21, 31, snap, crazy 8's and go fish. if you require the rules while
playing, a in-game help option will be available during your turn. The games save automatically, and will save
during the beginning of your turn. If you wish to load a previous game, select the load when prompted after 
selection your game.
NOTE***** program intended to be compiled with C++ 11 compiler.
*************************************************************************************************************