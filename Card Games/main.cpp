#include <iostream>
//#include <cppunit/CompilerOutputter.h>
//#include <cppunit/extensions/TestFactoryRegistry.h>
//#include <cppunit/ui/text/TestRunner.h>
#include "Game.h"
#include "Deck.h"

using namespace std;

int GetInteger()
{
    int inp_int;
    while(true)
    {
        cin >> inp_int;
        if (!cin.fail())
            break;
        else
        {
            cin.clear();
            cin.ignore(256,'\n');
            cout << "Please Enter valid value: ";
        }


    }
    return inp_int;

}

int main()
{
    int input;
    Game A;
    do
    {
     cout<<"Welcome!"<<endl;
     cout<<"Please Input the number of the game you wish to play"<<endl;
     cout<<" 1=GoFish      2=Snap     3=Crazy8's    4=TwentyOne     5=ThirtyOne"<<endl;
     cout<<" Enter 6 to exit: " ;
     input = GetInteger();
     if (input==6)
     {
        return 0;
     }
     else
     {
         A.Play(input);
     }
     }while (input!=6);

     //Cppunit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest();
    // Cppunit::textUi::TestRunner runner;
    // runner.addtest( suite );
   //  runner.setOutputter( new CppUnit::CompilerOutputter( &runner.result(),
//                                                       std::cerr ) );
 // bool wasSucessful = runner.run();
 // return wasSucessful ? 0 : 1;


}
