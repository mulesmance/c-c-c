#include "Game.h"
#include "TwentyOne.h"
#include "Snap.h"
#include "ThirtyOne.h"
#include "GoFish.h"
#include "Crazy8.h"
#include <iostream>

using namespace std;
/**
* Constructor for game object
*/
Game::Game()
{
    state=0;
};

/**
* Plays a game based on user input
* TODO: add functions that run the respective games
*/
void Game::Play(int i)
{
    state=i;
    //GoFish
    if (state==1)
    {
        GoFish game1;
        game1.Play_GoFish();
        state=0;
        return;
    }
    //Snap
    if (state==2)
    {
        snap game2;
        game2.play_snap();
        state=0;
        return;
    }
    //Crazy8's
    if (state==3)
    {
        Crazy8 game3;
        game3.Play_Crazy8();
        state=0;
        return;
    }
    //TwentyOne
    if (state==4)
    {
        TwentyOne game4;
        game4.play_21();
        state=0;
        return;
    }
    //ThirtyOne
    if (state==5)
    {
        ThirtyOne game5;
        game5.Play_31();
        state=0;
        return;
    }
};
