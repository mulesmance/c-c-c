#include "GoFish.h"
#include "Deck.h"
#include <iostream>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <math.h>
#include <conio.h>

using namespace std;

GoFish::GoFish()
{

}

GoFish::~GoFish()
{
    CompHand.clear();
    PlayerHand.clear();
}

std::vector<Card::card> GoFish::RemoveBook(std::vector<Card::card> RemHand, int faceToRem)
{
    int index = 0;
    while(index < RemHand.size())
    {
        if(RemHand.at(index).faces == faceToRem){
            RemHand.erase(RemHand.begin()+index);
            index = 0;
        }
        else
            index ++;
    }
    return RemHand;

}

bool GoFish::LoadPrvGame()
{
    if (D.LoadDeck("GoFish"))
    {
        CompHand=D.GetHand(3);
        PlayerHand=D.GetHand(2);
        return true;
    }
    else
        return false;
}

void GoFish::CreateHands()
{
    for (int i=0; i<7; i++)
    {
        CompHand.push_back(D.DrawCard(3));
        PlayerHand.push_back(D.DrawCard(2));
    }
}

int GoFish::CheckBook(std::vector<Card::card> ChkHand)
{
    int index = 0;
    int rtrn = 0;
    int face_cont[13] = {0};
    while (index < ChkHand.size())
    {
        for(int face = 0;face < 13;face ++)
        {
            if(face+1 == ChkHand.at(index).faces)
                face_cont[face] ++;
        }
        index ++;
    }
    index = 0;
    for(int face = 0;face < 13;face ++)
        {
            if(face_cont[face] == 4)
            {
                rtrn = face+1;
                while (index < ChkHand.size())
                {
                    if(ChkHand.at(index).faces == face+1)
                        D.SetDiscard(ChkHand.at(index));


                    index ++;

                }
            }
        }
        return rtrn;
}

void GoFish::PrintHelp(int choice)
{
    if (choice == 65)
            {
                cout<<"Objective: Make as many 'books' (sets of 4 matching faces) as possible."<<endl;
                cout<<"All Players will take turn asking each other about a card that they have "<<endl;
                cout<<"in their hand. If the other player has the matching card(s) they must "<<endl;
                cout<<"surrender it to the asking Player. Asking player will get another turn. " << endl;
                cout<<"If 2nd player does not have the said card, the asking player will 'Go Fish'"<<endl;
                cout<<"(draw a card from deck). If the drawn card is a match asking player gets to "<<endl;
                cout<<"ask again or its 2nd player turn."<<endl;
            }
}

void GoFish::SetExit(int choice)
{
    if(choice == 55)
        exit = true;
}


void GoFish::Play_GoFish()
{


    cout<<"Welcome to Go Fish!"<<endl;

    do
   {
        if(D.IsGameFile("GoFish")){
            cout<<"Load game? 1.yes 0.no ";
            load = GetInt();
            if (load==true)
            {
                if (LoadPrvGame())
                    load = true;
            }
        }
        if (load==false)
        {
            CreateHands();
        }
        CompAction=false;
        PlayerAction=true;

        while (winner == 0) {

            BookFace = CheckBook(PlayerHand);
            if(BookFace> 0){
                plyr_score ++;
                PlayerHand = RemoveBook(PlayerHand,BookFace);
            }
            BookFace = CheckBook(CompHand);
            if( BookFace > 0){
                comp_score ++;
                CompHand = RemoveBook(CompHand,BookFace);
            }
                   if (PlayerHand.size() == 0){
                temp3 = D.DrawCard(2);
                if(temp3.faces != 14)
                    PlayerHand.push_back(temp3);
                else{
                    winner == 1;
                    break;
                }
            }
            if (CompHand.size() == 0){
                temp3 = D.DrawCard(3);
                if(temp3.faces != 14)
                    CompHand.push_back(temp3);
                else{
                    winner == 1;
                    break;
                }
            }


            //temp=Starter[0];
            cout << "Your Score= " << plyr_score << " " << "Computer Score= " << comp_score << endl;
            cout << "Your Hand has = " << PlayerHand.size() << " " << "Computer's Hand has= " << CompHand.size() << endl;
            cout<<" your current hand is: "<<endl;

            for (int i=0; i<PlayerHand.size(); i++)
            {
                cout<<i+1<<". ";
                PrintCard(PlayerHand.at(i));
            }
            D.SaveDeck("GoFish");
             if(PlayerAction == true)
                {
                    cout<<"Enter Card Number to ask computer.  55. to exit, 65. for Help: ";
                    input = GetInt();
                    PrintHelp(input);
                    SetExit(input);
                    if(exit)
                        return;
                }
                if(CompAction == true)
                {
                    cout<<"Press 1 to proceed to computer's turn.  55. to exit, 65. for Help: ";
                    input = GetInt();
                    PrintHelp(input);
                    SetExit(input);
                    if(exit)
                        return;
                }
            if (PlayerAction == true)
            {

                while ((input < 1) || (input > PlayerHand.size()))
                {
                    cout << "Enter Card Number to ask computer.  55. to exit, 65. for Help:";
                    input = GetInt();
                    PrintHelp(input);
                    SetExit(input);
                    if(exit)
                        return;
                }
            }
            serch = 0;

            if(PlayerAction == true)
            {
                PlayerAction = false;
                while(serch < CompHand.size())
                {
                    if(CompHand.at(serch).faces == PlayerHand.at(input-1).faces)
                    {
                        D.SetPlayer(CompHand.at(serch), 2);
                        PlayerHand.push_back(CompHand.at(serch));
                        CompHand.erase(CompHand.begin()+serch);
                        serch = 0;
                        PlayerAction = true;
                        CompAction = false;

                    }
                    else
                        serch ++;
                }
                if(PlayerAction == true)
                    cout << "Computer handed over matching card(s)! \tYour Turn Again!" << endl;
            }
            if ((PlayerAction == false) && (CompAction == false))
            {
                cout << "Computer Does not have a match" << endl << "You had to Go Fish" << endl;
                temp = D.DrawCard(2);
                if(temp.faces == 14)
                    break;
                PlayerHand.push_back(temp);
                if(temp.faces == PlayerHand.at(input-1).faces)
                {
                    cout << "Newly fished card was a match!" << endl << "Player's Turn again" << endl;
                    PlayerAction = true;
                    CompAction = false;
                }
                else
                    CompAction = true;
            }

            else if((CompAction == true) && (input == 1))
            {
                serch = 0;
                CompAction = false;
                input = 1;
                if(comp_ask >= CompHand.size())
                    comp_ask = 0;
                cout << "Computer asked for: ";
                PrintCard(CompHand.at(comp_ask));

                while(serch < PlayerHand.size())
                {
                    if(PlayerHand.at(serch).faces == CompHand.at(comp_ask).faces)
                    {
                        D.SetPlayer(PlayerHand.at(serch), 3);
                        CompHand.push_back(PlayerHand.at(serch));
                        PlayerHand.erase(PlayerHand.begin()+serch);
                        serch = 0;
                        CompAction = true;
                        PlayerAction = false;

                    }
                    else
                        serch ++;
                }
                if(CompAction == true)
                    cout << "You handed over matching card(s)! \tComputer's Turn Again!" << endl;

                comp_ask ++;
            }
            else if(PlayerAction == false)
                cout << "Invalid Input\n";

            if ((CompAction == false) && PlayerAction == false)
            {
                cout << "You did not have a match" << endl << "Computer had to Go Fish" << endl;
                temp2 = D.DrawCard(3);
                if(temp2.faces == 14)
                    break;
                CompHand.push_back(temp2);
                if(temp2.faces == CompHand.at(input-1).faces)
                {
                    cout << "Newly fished card was a match!" << endl << "Computer's Turn again" << endl;
                    PlayerAction = false;
                    CompAction = true;
                }
                else
                    PlayerAction = true;
            }





            }

            if((temp.faces == 14) || (temp2.faces == 14)|| (temp3.faces == 14))
            {
                cout << "All of Deck has been used" << endl;
                if(comp_score > plyr_score)
                    cout << "Computer Wins" << endl;
                if(comp_score < plyr_score)
                    cout << "You Win!" << endl;
                winner = 1;
            }
            play = false;
            cout << "Press any key to go back to main menu:" << endl;
            getch();




  }while (play==true);
  return;
 //   cout << "hello" ;

}




//constructor

/*
GoFish::GoFish()
{

    DrawCard();

};

int CompSum = 5; //user and computer should start with five cards in their hands
int UserSum = 5;
int point;
string L = "GoFish";

void GoFish::play_gofish()
{
    cout << " Welcome to the game of Go Fish!!" << endl;
    bool play = true;
    do
    {
        bool load=false;
        cout<<"load save file? 1.yes 0.no"<<endl;
        cin>>load;
        Deck D; //generate the deck
        ShuffleDeck();
        std::vector<Card::card> CompHand;
        std::vector<Card::card> PlayerHand;


        if (load = true)
        {
            D.LoadDeck(L);
            CompHand=D.GetHand(3);
            int size1=CompHand.size();
            for (int i=0; i<size1; i++)
            {
                card temp=CompHand[i];
                CompSum+=temp.faces;
            }
            PlayerHand=D.GetHand(2);
            size1=PlayerHand.size();
            cout<<"size: "<<size1<<endl;
            for (int i=0; i<size1; i++)
            {
                card temp=PlayerHand[i];
                UserSum+=temp.faces;
            }
        }

        cout<<"deck created.."<<endl;

//if (Deck().empty &&) {

    }  while (play == true);   //game will not take place if play!=true


}
void GoFish::ask()
{
    string input;
cout << "Player turn: Do you have any" << endl;
    cin >> input;
if (input == "yes")
    {
        stealCard();
        point++;
    }
else if (input == "no")
    {
GoFish();
        UserSum++; //Player has to draw a card if the card they asked for is not in hand
    }
}

//if player has the card that is asked, opponent gets to take it
void GoFish::stealCard()
{
  CompSum--;
}

//new pile of cards that are put away
void GoFish::AddToSmallPile()
{
//    bool steal = stealCard();
    vector <Card> discard;
//    if (steal = true)
    {
        //  discard.push_back<card>; //push the card unto discarded piles
    }
}

//if any of the players run out of cards then they pick 5 new cards
void DrawCard()
{
    if (CompSum = 0)
    {
        DrawCard();
        DrawCard();
        DrawCard();
        DrawCard();
        DrawCard();
    }
    if (UserSum = 0)
    {
        DrawCard();
        DrawCard();
        DrawCard();
        DrawCard();
        DrawCard();
    }
}
*/
