#include "TwentyOne.h"
#include "Deck.h"
#include "Card.h"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <conio.h>
using namespace std;

TwentyOne::TwentyOne()
{

};

void TwentyOne::play_21()
{
    Deck D;
    std::vector<Card::card> CompHand;
    std::vector<Card::card> PlayerHand;
    int CompSum=0;
    int UserSum=0;
    string L = "TwentyOne";
    cout<<"Welcome to 21!"<<endl;
    bool play=true;
        bool load=false;
    if(D.IsGameFile(L)){
        cout<<"load save file? 1.yes 0.no: ";
        load = GetInt();

        if(load==true)
        {
            if (D.LoadDeck(L))
                load = true;
            else
                load = false;
        }
    }
    if (load == true)   {
            CompHand=D.GetHand(3);
            int size1=CompHand.size();
            for (int i=0; i<size1; i++)
            {
                card temp=CompHand[i];
                CompSum+=temp.faces;
            }
            PlayerHand=D.GetHand(2);
            size1=PlayerHand.size();
            cout<<"size: "<<size1<<endl;
            for (int i=0; i<size1; i++)
            {
                card temp=PlayerHand[i];
                UserSum+=temp.faces;
            }

    }
    if(load == false)
    {
        cout << "Starting With a new Deck! \n";
    }
    do
    {

        cout<<"deck created.."<<endl;
        cout<<"computer goes first.."<<endl;
        int winner;
        card Compcard;
        card UserCard;
        bool UserStick=false;
        bool CompStick=false;
        bool CompLoss=false;
        bool PlayerTurn=false;
        int input=0;
        winner=0;
        cout<<"computer sum is: "<<CompSum<<endl;
        cout<<"your sum is: "<<UserSum<<endl;
        while (winner==0)
        {
            if(CompStick==false)
            {
            if(CompSum<15 && CompLoss==false)
              {
                  Compcard=D.DrawCard(3);
                  cout<<"computer has drawn a card with the value of: "<<Compcard.faces<<endl;
                  CompSum+=Compcard.faces;
              }
              if(CompSum>21)
              {
                  cout<<"computer has gone over 21. You win!"<<endl;
                  CompLoss=true;
                  winner=1;
              }
              if(CompSum>=15 && CompLoss==false)
              {
                 CompStick=true;
              }

             }
            if(CompStick==true)
             {
                cout<<"Computer has stuck."<<endl;
             }
             if (CompLoss==false)
             {


             cout<<"The Computers current sum is: "<<CompSum<<endl;
             cout<<"Your current sum is: "<<UserSum<<endl;
             if (UserStick==true)
              {
                cout<<"you have stuck"<<endl;
              }
             if (UserStick==false)
              {
               D.SaveDeck(L);
               PlayerTurn=false;
               while (PlayerTurn==false)
               {
               cout<<"Your options are: 1.Draw  2.Stick 3.Help 4.Exit"<<endl;
               cout<<"Enter the number of your choice....: ";
               input = GetInt();
               if (input==4)
               {
                return;
               }
               if (input==3)
               {
                cout<<"Objective: reach a sum closest to 21 without exceeding it."<<endl;
                cout<<"On your turn you can either draw a new card or stick."<<endl;
                cout<<"Once both players have stuck, the sums are compared and the closest to 21 wins!"<<endl;
                cout<<"Going over 21 results in an instant loss."<<endl;
               }
               if (input==1)
               {
                  UserCard=D.DrawCard(2);
                  cout<<"you have drawn a card value of: "<<UserCard.faces<<endl;
                  UserSum+= UserCard.faces;
                  if(UserSum>21)
                  {
                     cout<<"you have gone over 21. Computer wins!"<<endl;
                     winner=1;
                  }
                  PlayerTurn=true;
               }
               if (input==2)
               {
                 UserStick=true;
                 cout<<"you have stuck"<<endl;
                 PlayerTurn=true;
               }
               }
              }
              if (CompStick==true && UserStick==true)
              {
                 if (UserSum>=CompSum)
                 {
                     cout<<"You Win!"<<endl;
                 }
                 if (CompSum>UserSum)
                 {
                     cout<<"Computer Wins!"<<endl;
                 }
                 winner=1;
              }
             }

        };
        //cout<<"play again? 1.yes 0.no"<<endl;
        //cin>>play;
        play = false;
        cout << "Press any key to go back to main menu:" << endl;
        getch();
    } while (play==true);

}
