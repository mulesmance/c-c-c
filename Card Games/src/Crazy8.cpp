#include "Crazy8.h"
#include "Deck.h"
#include "Card.h"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <conio.h>

using namespace std;

Crazy8::Crazy8()
{

}

void Crazy8::Play_Crazy8()
{
    cout<<"Welcome to Crazy8!"<<endl;
    bool play=true;
    do
    {
        string L= "Crazy8";
        Deck D;
        std::vector<Card::card> CompHand;
        std::vector<Card::card> PlayerHand;
        std::vector<Card::card> Starter;
        int load=0;
        int winner=0;
        int size1=0;
        int input=0;
        card temp;
        card temp2;
        card temp3;
        bool PlayerAction=false;
        bool CompAction=false;
        bool DeckEmpty=false;
        if(D.IsGameFile(L)){
            cout<<"Load game? 1.yes 0.no: ";
            load = GetInt();
            if (load==true)
            {
                if (D.LoadDeck(L));
                else
                    load = false;
            }
        }
        if (load == true){
            CompHand=D.GetHand(3);
            PlayerHand=D.GetHand(2);
            Starter=D.GetHand(4);
            }

        if (load==false)
        {
            for (int i=0; i<5; i++)
            {
                temp=D.DrawCard(3);
                CompHand.push_back(temp);
            }
            for (int i=0; i<5; i++)
            {
                temp=D.DrawCard(2);
                PlayerHand.push_back(temp);
            }
            temp=D.DrawCard(4);
            Starter.push_back(temp);
        }
        do
        {
            CompAction=false;
            PlayerAction=false;
            temp=Starter[0];
            cout<<"The current starter card is: ";
            PrintCard(temp);
            cout<<" your current hand is: "<<endl;
            size1=PlayerHand.size();
            for (int i=0; i<size1; i++)
            {
                temp=PlayerHand[i];
                cout<<i+1<<". ";
                PrintCard(temp);
            }

            do
            {
            D.SaveDeck(L);
            cout<<"options: 1.place a card 2.draw a card 3.help 4.exit: ";
            input = GetInt();
            if (input==3)
            {
                cout<<"Objective: Get rid of your hand."<<endl;
                cout<<"To remove a card from your hand, the card must match the current starter card's face or suit."<<endl;
                cout<<"If no move is possible you must draw a card and add it to your hand."<<endl;
                cout<<"If the current starter card is an 8, you can remove any card from your hand."<<endl;
                cout<<"Removing a card from a hand replaces the starter with the discarded card."<<endl;
            }
            if (input==4)
            {
                return;
            }
            if (input==1 && PlayerAction==false)
            {
                cout<<"Which card will you add? ";
                input = GetInt();
                input=input-1;
                temp=PlayerHand[input];
                temp2=Starter[0];
                if (temp.faces==temp2.faces || temp.CardSuits==temp2.CardSuits || temp.faces==8)
                {
                    D.SetDiscard(temp2);
                    PlayerHand.erase(PlayerHand.begin()+input);
                    Starter[0]=temp;
                    D.SetPlayer(temp, 4);
                    PlayerAction=true;
                }
                else
                {
                    cout<<"Invalid move!"<<endl;
                    PlayerAction=false;
                    input=1;
                }
            }
            if (input==2 && PlayerAction==false)
            {
                temp=D.DrawCard(2);
                if (temp.faces==14)
                {
                    cout<<"deck is empty! Winner will be determined by size of hand."<<endl;
                    PlayerAction=true;
                    DeckEmpty=true;
                }
                else
                {
                PlayerHand.push_back(temp);
                PlayerAction=true;
                }
            }
            }while (PlayerAction==false);
            if (PlayerHand.empty()==true)
            {
                cout<<"Your hand is empty. You Win!"<<endl;
                winner=1;
            }
            if (winner==0)
            {
            cout<<"computers turn..."<<endl;
            temp=Starter[0];
            size1=CompHand.size();
            for (int i=0; i<size1; i++)
            {
                temp2=CompHand[i];
                if ((temp2.faces==temp.faces || temp2.CardSuits==temp.CardSuits || temp.faces==8) && CompAction==false)
                {
                    D.SetDiscard(temp);
                    CompHand.erase(CompHand.begin()+i);
                    Starter[0]=temp2;
                    D.SetPlayer(temp2, 4);
                    CompAction=true;
                }
            }
            if (CompAction==false)
            {
                cout<<"computer is drawing a card."<<endl;
                temp=D.DrawCard(3);
                if (temp.faces==14)
                {
                    cout<<"Deck is empty! Winner will be determined by size of hand."<<endl;
                    CompAction=true;
                    DeckEmpty=true;
                }
                CompHand.push_back(temp);
                CompAction=true;
            }
            if (CompHand.empty()==true)
            {
                cout<<"computer has and empty hand. Computer wins!"<<endl;
                winner=1;
            }
            if (DeckEmpty==true)
            {
                if (PlayerHand.size()>CompHand.size())
                    {
                        cout<<"Your hand is larger than the computers. Computer Wins!"<<endl;
                        winner=1;
                    }
                    if (PlayerHand.size()<CompHand.size())
                    {
                        cout<<"Your hand is smaller than the computers. You Win!"<<endl;
                        winner=1;
                    }
                    if (PlayerHand.size()==CompHand.size())
                    {
                        cout<<"Your hand and the computers hand have the same number of cards. Its a draw!"<<endl;
                        winner=1;
                    }
            }
            }

        }while (winner==0);
        //cout<<"Play Again? 1.yes 0.no"<<endl;
        //cin>>play;
        play = false;
        cout << "Press any key to go back to main menu:" << endl;
        getch();

    } while (play==true);
    return;
}

