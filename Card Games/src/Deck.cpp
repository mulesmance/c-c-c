#include "Deck.h"
#include "Card.h"
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <iostream>
#include <ctime>
#include <fstream>
#include <sstream>


using namespace std;
/**
* Creates a deck of cards, represented by the vector active, and then shuffles them to be randomized.
*/

static int cur_index;

Deck::Deck()
{
    CreatDeck();
    ShuffleDeck();

    /*All code between these code lines is for domo only
    * */
    //card d_card;
    //DrawCard(2);  //Player Hand is 2, Computer hand is 3
    //d_card.faces = 10;
    //d_card.CardSuits = Card_suits(3);
    //SetDiscard(d_card);
    //SaveDeck("TwentyOne");
    /*All code between these code lines is for domo only */

}

/**
* Creates a deck of cards
* @pram: n_deck <vector>
* @return None
*/
void Deck::CreatDeck(){

    Card::Card_suits suit_, c_suit;
    int face = 1;
    int suit = 0;
    s_card cur_card;
    suit_ = Card_suits(0);
    for(int c = 0; c < 52; c++)
    {
        if (face%14 == 0) {
            face = 1;
            suit ++;
            if (suit ==1)
                suit_ = Card_suits(1);
            else if (suit ==2)
                suit_ = Card_suits(2);
            else if (suit == 3)
                suit_ = Card_suits(3);
        }
        cur_card.g_card.faces = face;
        cur_card.g_card.CardSuits = suit_;
        cur_card.status = 1;
        n_deck.push_back(cur_card);
        face ++;
    }
    cur_index = 0;

}

/**
* Shuffle the deck
* @pram: n_deck <vector>
* @return None
*/
void Deck::ShuffleDeck(){
    int cur_pos = 0;
    for(int i = 0; i < 52; i++)
    {
        int new_pos = (rand() + time(0))%52;
        swap(n_deck[cur_pos],n_deck[new_pos]);
    }
}

/**
* Creates a deck of cards
* @pram: n_deck <vector>
* @output prints the deck
* @TODO: Remove Before release
*/
void Deck::printDeck()
{
    int num = n_deck.size();
    while(num > 0)
    {
        cout << n_deck.at(num-1).g_card.CardSuits << " "
                << n_deck.at(num-1).g_card.faces << " "
                << n_deck.at(num-1).status <<endl;
        num --;
    }
}

Card::card Deck::DrawCard(int id)
{
    int card_drawn = 0;
    card rtrn_card;
    rtrn_card.faces = 14;
    while(cur_index != 52)
    {
        if(n_deck.at(cur_index).status == 1)
        {
            rtrn_card.CardSuits     = n_deck.at(cur_index).g_card.CardSuits;
            rtrn_card.faces         = n_deck.at(cur_index).g_card.faces;
            n_deck.at(cur_index).status  = id;
            break;
        }
        cur_index ++;
    }
    return rtrn_card;
}
void Deck::SetPlayer(card Set_card, int id)
{
    int index = 0;
    while (index != 52)
    {
        if(n_deck.at(index).g_card.CardSuits == Set_card.CardSuits &&
           n_deck.at(index).g_card.faces == Set_card.faces)
        {
            n_deck.at(index).status = id;
            break;
        }
        index ++;
    }
}
void Deck::SetDiscard(card Discard_card)
{
    int index = 0;
    while (index != 52)
    {
        if(n_deck.at(index).g_card.CardSuits == Discard_card.CardSuits &&
           n_deck.at(index).g_card.faces == Discard_card.faces)
        {
            n_deck.at(index).status = 5;
            break;
        }
        index ++;
    }
}

bool Deck::SaveDeck(std::string deck_name)
{
    string filename = "gamedeck";
    filename.append(".");
    filename.append(deck_name);
    ofstream deck_file;
    int index = 0;
    try{
        deck_file.open(filename.c_str());
        for(int index = 0; index < n_deck.size(); index++)
        {
            deck_file << ","<< n_deck.at(index).g_card.CardSuits << ","
                      << n_deck.at(index).g_card.faces << ","
                      << n_deck.at(index).status << ","<< "a"<< "\n";
        }
        deck_file.close();
    }
    catch (...){
        throw EOF;
        return false;
    }
    return true;
}
bool Deck::LoadDeck(std::string deck_name)
{
    string filename = "gamedeck";
    string readline, n_str;
    char value[3];
    filename.append(".");
    filename.append(deck_name);
    ifstream deck_file;
    int index = 0;
    int num;
    int col = 0;
    char num_char;
    int fields[3];
    try{
        deck_file.open(filename.c_str());
        while (deck_file.good())
        {
            std::getline(deck_file, readline, ',');

            if(readline != "\0")
                //cout << readline << " ";

            if(readline[0] == 'a')
            {
                //cout << endl;
                col = 0;
                n_deck.at(index).g_card.CardSuits = Card_suits(fields[0]);
                n_deck.at(index).g_card.faces = fields[1];
                n_deck.at(index).status = fields[2];
                index ++;
            }

            else
            {
                try
                {
                    num = atoi(readline.c_str());
                    //cout << num << " ";

                    fields[col] = num;

                    if(col == 0){
                        if((fields[col] < 0) || (fields[col] > 3)){
                            index --;
                        }
                    }
                    else if(col == 1){
                        if((fields[col] < 1) || (fields[col] > 13)){
                            index --;
                        }
                    }
                    else if(col == 2){
                        if((fields[col] < 1) || (fields[col] > 5)){
                            index --;
                        }
                    }
                    col ++;

                }
                catch(...){
                    throw "Error \n"; }
            }


            /*if (readline[0] != '\0'){

            cout << readline[0] << " ";

            if(readline[0] == 'a')
            {
                col = 0;
                n_deck.at(index).g_card.CardSuits = Card_suits(fields[0]);
                n_deck.at(index).g_card.faces = fields[1];
                n_deck.at(index).status = fields[2];
                index ++;
            }
            else{
                try{

                        //num = readline[0] - '0';
                    num = 0;
                    fields[col] = num;

                    if(col == 0){
                        if((fields[col] < 0) || (fields[col] > 3)){
                            index --;
                        }
                    }
                    else if(col == 1){
                        if((fields[col] < 1) || (fields[col] > 13)){
                            index --;
                        }
                    }
                    else if(col == 2){
                        if((fields[col] < 1) || (fields[col] > 5)){
                            index --;
                        }
                    }
                    col ++;

                }
                catch(...)
                {
                    break;
                    throw "error";
                }
            }
            }*/
        }

        deck_file.close();
    }
    catch (...){
        throw EOF;
    }
    if (index != 52) return false;
    cur_index = 0;
    return true;
}
bool Deck::IsGameFile(std::string deck_name)
{
    string filename = "gamedeck";
    filename.append(".");
    filename.append(deck_name);
    std::ifstream deck_file(filename.c_str());
    return deck_file.good();
}

std::vector<Card::card> Deck::GetHand(int id)
{
    std::vector<card> hand;
    card temp_card;
    int index = 0;
    while(index < n_deck.size())
    {
        if(n_deck.at(index).status == id)
        {
            temp_card.faces = n_deck.at(index).g_card.faces;
            temp_card.CardSuits = n_deck.at(index).g_card.CardSuits;
            hand.push_back(temp_card);
        }
        index ++;
    }
    return hand;
}

int Deck::GetInt()
{
    int inp_int;
    while(true)
    {
        cin >> inp_int;
        if (!cin.fail())
            break;
        else
        {
            cin.clear();
            cin.ignore(256,'\n');
            cout << "Please Enter valid value: ";
        }


    }
    return inp_int;

}

void Deck::PrintCard(card card2print)
{
    std::string suit_,face_;
    int is_name = 1;
    switch(card2print.faces)
        {
        case 1:
            face_ = "Ace";
            break;
        case 11:
            face_ = "Jack";
            break;
        case 12:
            face_ = "Queen";
            break;
        case 13:
            face_ = "King";
            break;
        default:
            is_name = 0;
        }
        switch(card2print.CardSuits)
        {
        case hearts:
            suit_ = "Hearts";
            break;
        case diamonds:
            suit_ = "Diamonds";
            break;
        case clubs:
            suit_ = "Clubs";
            break;
        case spades:
            suit_ = "Spades";
            break;
        }

        if(is_name == 1)
            cout << face_ << " of " << suit_ << endl;
        else
            cout << card2print.faces << "\t of " << suit_ << endl;
}

 Deck::~Deck()
{
    n_deck.clear();
}
