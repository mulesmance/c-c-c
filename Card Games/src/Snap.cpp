#include "Snap.h"
#include "Deck.h"
#include "Card.h"
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <conio.h>

using namespace std;


//constructor
snap::snap() {}


void snap::play_snap()
{
    cout << " Welcome to the game of Snap!" << endl;
    cout << "How to play: \n\n";
    int choice;
    cout << "Objective: Each player will have a stack and there will be a third stack called Snap pool;" << endl;
    cout << "in order to win users must keep flipping their cards until they get a card that matches the card on "  << endl;
    cout << "the first pile of the snap pool. Whoever shouts 'Snap pool' first gets to take the Snap pool and wins." << endl;

    bool play = true;
    do
    {
        Deck D;
        ShuffleDeck();
        std::vector<Card::card> playerPool;
        std::vector<Card::card> computerPool;
        int winner = 0;
        card temp;
        card temp2;
        temp=D.DrawCard(5);
        temp2=temp;
        while (temp2.faces!=14)
        {
            temp2=D.DrawCard(2);
            playerPool.push_back(temp2);
            temp2=D.DrawCard(3);
            computerPool.push_back(temp2);

        }
        int size1=playerPool.size();
        int size2=computerPool.size();

        cout << "Players are flipping there cards..." << endl;
        //sleep(1);
        //for loop to make sure that each player gets their turn

        for (int n=0; n < playerPool.size(); n++)
        {
            //if the player pool finds a match with the snap pool, they win.
            temp2=playerPool[n];
            if(temp2.CardSuits==temp.CardSuits && winner == 0)
            {
                cout <<"Player shouts 'Snap Pool!!'..takes the snap pool!" << endl;
                cout << "Winner is Player!"<<endl;
                winner=1;
            }
            //vice verca if the computer finds a match with the snap pool, it is the winner.
            temp2=computerPool[n];
            if (temp2.CardSuits==temp.CardSuits && winner == 0)
            {
                cout <<"Computer shouts 'Snap Pool!!'..takes the snap pool!" << endl;
                cout <<"Winner is Computer!"<<endl;
                winner=1;
            }
            if (winner==0)
            {
                cout<<"no match, flipping again....."<<endl;
            }
        }
        while (winner==0);
        //cout<< endl <<"Play Again? 1.yes 0.no"<<endl;
        //cin>>play;
        play = false;
        cout << "Press any key to go back to main menu:" << endl;
        getch();
    }
    while (play == true);             //game will not take place if play!=true
}






