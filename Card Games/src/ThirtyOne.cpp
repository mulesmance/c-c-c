#include "ThirtyOne.h"
#include "Deck.h"
#include "Card.h"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <conio.h>

using namespace std;

ThirtyOne::ThirtyOne()
{

};

void ThirtyOne::Play_31()
{
    bool play=true;
    do
    {
    string L= "ThirtyOne";
    cout<<"welcome to 31!"<<endl;
    Deck D;
    std::vector<Card::card> CompHand;
    std::vector<Card::card> PlayerHand;
    std::vector<Card::card> Window;
    int load=0;
    int UserSum=0;
    int CompSum=0;
    int winner=0;
    int input=0;
    int input2=0;
    card temp;
    card temp2;
    card temp3;
    card temp4;
    bool delayCompKnock=false;
    bool CompKnock=false;
    bool UserKnock=false;
    bool compAction=false;
    bool playerAction=false;
    if(D.IsGameFile(L))
    {
         cout<<"Load game? 1.yes 0.no: ";
         load = GetInt();

         if (load==1)
        {
          if (D.LoadDeck(L));
          else
            load = false;
        }
    }
     if(load == true){
          CompHand=D.GetHand(3);
          int size1=CompHand.size();
          for (int i=0; i<size1; i++)
          {
              temp2=CompHand[i];
              CompSum+=temp2.faces;
          }
          PlayerHand=D.GetHand(2);
          size1=PlayerHand.size();
          for (int i=0; i<size1; i++)
          {
              temp2=PlayerHand[i];
              UserSum+=temp2.faces;
          }
          Window=D.GetHand(4);

        }

        if (load==0)
        {
            for (int i=0; i<3; i++)
            {
                temp3=D.DrawCard(3);
                CompHand.push_back(temp3);
            }
            for (int i=0; i<3; i++)
            {
                temp3=D.DrawCard(2);
                PlayerHand.push_back(temp3);
            }
            temp3=D.DrawCard(4);
            Window.push_back(temp3);
            temp3=D.DrawCard(4);
            Window.push_back(temp3);
            temp3=D.DrawCard(4);
            Window.push_back(temp3);

        }



    do
    {
        compAction=false;
        playerAction=false;
        cout<<"Your hand is:"<<endl;
        for (int i=0; i<PlayerHand.size(); i++)
            {
                cout<<i+1<<". ";
                PrintCard(PlayerHand.at(i));
            }
        cout<<"The window is: "<<endl;
        for (int i=0; i<Window.size(); i++)
            {
                cout<<i+1<<". ";
                PrintCard(Window.at(i));
            }
        D.SaveDeck(L);
        if (delayCompKnock==true)
        {
            cout<<"last turn!!!"<<endl;
            CompKnock=true;
        }
        while (playerAction==false)
        {
        cout<<"options: 1.Swap card one.  2.Swap card two. 3.Swap card three. 4.Knock 5.Help 6.Exit: ";
        input = GetInt();
        if (input==5)
        {
            cout<<"Game Objective: Reach closest to 31 without exceeding it. Your sum is calculated from the face values of one suit in your hand."<<endl;
            cout<<"You will have a hand of three cards. During your turn you will either switch a card from your hand to the window or Knock."<<endl;
            cout<<"Once you knock, the computer will have one more turn, then you will pick the suit from which you will add your sum."<<endl;
            cout<<"The player with the closest sum to 31 wins!"<<endl;

        }
        if (input==6)
        {
            return;
        }
        if (input==1)
        {
            cout<<"swap with which window card? ";
            input2 = GetInt();
            temp=PlayerHand[0];
            if (input2==1)
            {
                temp2=Window[0];
                D.SetPlayer(temp2, 2);
                D.SetPlayer(temp, 4);
                PlayerHand.erase(PlayerHand.begin());
                PlayerHand.insert(PlayerHand.begin(),temp2);
                Window.erase(Window.begin());
                Window.insert(Window.begin(),temp);
                playerAction=true;
            }
            if (input2==2)
            {
                temp2=Window[1];
                D.SetPlayer(temp2, 2);
                D.SetPlayer(temp, 4);
                PlayerHand.erase(PlayerHand.begin());
                PlayerHand.insert(PlayerHand.begin(),temp2);
                Window.erase(Window.begin()+1);
                Window.insert(Window.begin()+1,temp);
                playerAction=true;
            }
            if (input2==3)
            {
                temp2=Window[2];
                D.SetPlayer(temp2, 2);
                D.SetPlayer(temp, 4);
                PlayerHand.erase(PlayerHand.begin());
                PlayerHand.insert(PlayerHand.begin(),temp2);
                Window.erase(Window.begin()+2);
                Window.insert(Window.begin()+2,temp);
                playerAction=true;
            }
            if(input2!=1 && input2!=2 && input2!=3)
            {
                cout<<"Invalid option!"<<endl;
            }


        }
        if (input==2)
        {
            cout<<"swap with which window card? ";
            input2 = GetInt();
            temp=PlayerHand[1];
              if (input2==1)
            {
                temp2=Window[0];
                D.SetPlayer(temp2, 2);
                D.SetPlayer(temp, 4);
                PlayerHand.erase(PlayerHand.begin()+1);
                PlayerHand.insert(PlayerHand.begin()+1,temp2);
                Window.erase(Window.begin());
                Window.insert(Window.begin(),temp);
                playerAction=true;
            }
            if (input2==2)
            {
                temp2=Window[1];
                D.SetPlayer(temp2, 2);
                D.SetPlayer(temp, 4);
                PlayerHand.erase(PlayerHand.begin()+1);
                PlayerHand.insert(PlayerHand.begin()+1,temp2);
                Window.erase(Window.begin()+1);
                Window.insert(Window.begin()+1,temp);
                playerAction=true;
            }
            if (input2==3)
            {
                temp2=Window[2];
                D.SetPlayer(temp2, 2);
                D.SetPlayer(temp, 4);
                PlayerHand.erase(PlayerHand.begin()+1);
                PlayerHand.insert(PlayerHand.begin()+1,temp2);
                Window.erase(Window.begin()+2);
                Window.insert(Window.begin()+2,temp);
                playerAction=true;
            }
            if(input2!=1 && input2!=2 && input2!=3)
            {
                cout<<"Invalid option!"<<endl;
            }

        }
        if (input==3)
        {
            cout<<"swap with which window card? ";
            input2= GetInt();
            temp=PlayerHand[2];
                if (input2==1)
            {
                temp2=Window[0];
                D.SetPlayer(temp2, 2);
                D.SetPlayer(temp, 4);
                PlayerHand.erase(PlayerHand.begin()+2);
                PlayerHand.insert(PlayerHand.begin()+2,temp2);
                Window.erase(Window.begin());
                Window.insert(Window.begin(),temp);
                playerAction=true;
            }
            if (input2==2)
            {
                temp2=Window[1];
                D.SetPlayer(temp2, 2);
                D.SetPlayer(temp, 4);
                PlayerHand.erase(PlayerHand.begin()+2);
                PlayerHand.insert(PlayerHand.begin()+2,temp2);
                Window.erase(Window.begin()+1);
                Window.insert(Window.begin()+1,temp);
                playerAction=true;
            }
            if (input2==3)
            {
                temp2=Window[2];
                D.SetPlayer(temp2, 2);
                D.SetPlayer(temp, 4);
                PlayerHand.erase(PlayerHand.begin()+2);
                PlayerHand.insert(PlayerHand.begin()+2,temp2);
                Window.erase(Window.begin()+2);
                Window.insert(Window.begin()+2,temp);
                playerAction=true;
            }
            if(input2!=1 && input2!=2 && input2!=3)
            {
                cout<<"Invalid option!"<<endl;
            }

        }
        if (input==4)
        {
            cout<<"you have knocked."<<endl;
            UserKnock=true;
            playerAction=true;

        }
        }
        cout<<"computers turn.."<<endl;
        temp=CompHand[0];
        temp2=CompHand[1];
        temp3=CompHand[2];
        if (temp.CardSuits==temp2.CardSuits && temp.CardSuits==temp3.CardSuits && CompKnock==false)
        {
            delayCompKnock=true;
            cout<<"Computer has knocked! you will have one more turn."<<endl;
            compAction=true;

        }
        if(temp.CardSuits!=temp2.CardSuits && CompKnock==false && compAction==false)
        {
            temp4=Window[0];
            if (temp4.CardSuits==temp.CardSuits)
            {
                D.SetPlayer(temp4, 3);
                D.SetPlayer(temp2, 4);
                CompHand.erase(CompHand.begin()+1);
                CompHand.insert(CompHand.begin()+1, temp4);
                Window.erase(Window.begin());
                Window.insert(Window.begin(), temp2);
                compAction=true;
            }
            temp4=Window[1];
            if (temp4.CardSuits==temp.CardSuits && compAction==false)
            {
                D.SetPlayer(temp4, 3);
                D.SetPlayer(temp2, 4);
                CompHand.erase(CompHand.begin()+1);
                CompHand.insert(CompHand.begin()+1, temp4);
                Window.erase(Window.begin()+1);
                Window.insert(Window.begin()+1, temp2);
                compAction=true;
            }
            temp4=Window[2];
            if (temp4.CardSuits==temp.CardSuits && compAction==false)
            {
                D.SetPlayer(temp4, 3);
                D.SetPlayer(temp2, 4);
                CompHand.erase(CompHand.begin()+1);
                CompHand.insert(CompHand.begin()+1, temp4);
                Window.erase(Window.begin()+2);
                Window.insert(Window.begin()+2, temp2);
                compAction=true;
            }
        }
        if(temp.CardSuits!=temp3.CardSuits && CompKnock==false && compAction==false)
        {
            temp4=Window[0];
            if (temp4.CardSuits==temp.CardSuits)
            {
                D.SetPlayer(temp4, 3);
                D.SetPlayer(temp3, 4);
                CompHand.erase(CompHand.begin()+2);
                CompHand.insert(CompHand.begin()+2, temp4);
                Window.erase(Window.begin());
                Window.insert(Window.begin(), temp3);
                compAction=true;
            }
            temp4=Window[1];
            if (temp4.CardSuits==temp.CardSuits && compAction==false)
            {
                D.SetPlayer(temp4, 3);
                D.SetPlayer(temp3, 4);
                CompHand.erase(CompHand.begin()+2);
                CompHand.insert(CompHand.begin()+2, temp4);
                Window.erase(Window.begin()+1);
                Window.insert(Window.begin()+1, temp3);
                compAction=true;
            }
            temp4=Window[2];
            if (temp4.CardSuits==temp.CardSuits && compAction==false)
            {
                D.SetPlayer(temp4, 3);
                D.SetPlayer(temp3, 4);
                CompHand.erase(CompHand.begin()+2);
                CompHand.insert(CompHand.begin()+2, temp4);
                Window.erase(Window.begin()+2);
                Window.insert(Window.begin()+2, temp3);
                compAction=true;
            }
        }
        if (UserKnock==true || CompKnock==true)
        {
            cout<<"pick the card thats suit you will use: "<<endl;
            cout<<"Your hand is:"<<endl;
            temp=PlayerHand[0];
            cout<<"1."<<temp.faces<<" of ";
             if (temp.CardSuits==0)
            {
                cout<<"Hearts"<<endl;
            }
            if (temp.CardSuits==1)
            {
                cout<<"Spades"<<endl;
            }
            if (temp.CardSuits==2)
            {
                cout<<"Clubs"<<endl;
            }
            if (temp.CardSuits==3)
            {
                cout<<"Diamonds"<<endl;
            }
            temp=PlayerHand[1];
            cout<<"2."<<temp.faces<<" of ";
             if (temp.CardSuits==0)
            {
                cout<<"Hearts"<<endl;
            }
            if (temp.CardSuits==1)
            {
                cout<<"Spades"<<endl;
            }
            if (temp.CardSuits==2)
            {
                cout<<"Clubs"<<endl;
            }
            if (temp.CardSuits==3)
            {
                cout<<"Diamonds"<<endl;
            }
            temp=PlayerHand[2];
            cout<<"3."<<temp.faces<<" of ";
             if (temp.CardSuits==0)
            {
                cout<<"Hearts"<<endl;
            }
            if (temp.CardSuits==1)
            {
                cout<<"Spades"<<endl;
            }
            if (temp.CardSuits==2)
            {
                cout<<"Clubs"<<endl;
            }
            if (temp.CardSuits==3)
            {
                cout<<"Diamonds"<<endl;
            }
            input = GetInt();
            if (input==1)
            {
                temp=PlayerHand[0];
                temp2=PlayerHand[1];
                temp3=PlayerHand[2];
            }
            if (input==2)
            {
                temp=PlayerHand[1];
                temp2=PlayerHand[0];
                temp3=PlayerHand[2];
            }
            if (input==3)
            {
                temp=PlayerHand[2];
                temp2=PlayerHand[0];
                temp3=PlayerHand[1];
            }
            UserSum+=temp.faces;
            if(temp.CardSuits==temp2.CardSuits)
            {
                UserSum+=temp2.faces;
            }
            if(temp.CardSuits==temp3.CardSuits)
            {
                UserSum+=temp3.faces;
            }
            cout<<"Your sum is: "<<UserSum<<endl;
            if(UserSum>31)
            {
                cout<<"you have gone over 31! Computer wins!"<<endl;
                winner=1;
            }
            temp=CompHand[0];
            temp2=CompHand[1];
            temp3=CompHand[2];
            CompSum+=temp.faces;
            if (temp.CardSuits==temp2.CardSuits)
            {
                CompSum+=temp2.faces;
            }
            if (temp.CardSuits==temp2.CardSuits)
            {
                CompSum+=temp3.faces;
            }
            cout<<"The computers sum is: "<<CompSum<<endl;
            if(CompSum>31 && winner==0)
            {
                cout<<"Computer has gone over 31! you win!"<<endl;
                winner=1;
            }
            cout<<"Comparing sums....."<<endl;
            if (UserSum>=CompSum && winner==0)
            {
                cout<<"You Win!"<<endl;
                winner=1;
            }
            if (CompSum>UserSum && winner==0)
            {
                cout<<"Computer Wins!"<<endl;
                winner=1;
            }
        }



    }while (winner==0);
    //cout<<"Play Again? 1.yes 0.no"<<endl;
    //cin>>play;
    play = false;
    cout << "Press any key to go back to main menu:" << endl;
    getch();
    }while(play==true);

}
