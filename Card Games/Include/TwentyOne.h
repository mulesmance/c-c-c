
#ifndef TWENTYONE_H
#define TWENTYONE_H
#include "Deck.h"

class TwentyOne:public Deck{
public:

    TwentyOne();
    /**
    *Runs the game crazy8. returns to the main menu once complete and saves game automatically. can load previous games.
    */
    void play_21();
};
#endif // 21_H
