#ifndef DECK_H
#define DECK_H
#include <vector>
#include "Card.h"
#include <string>


class Deck:public Card
{
public:
    struct s_card {
        card g_card;
        int status;
    };
    /**
    * constructor
    */
    Deck();
    ~Deck();
    /**
    * outputs deck to a text file that can be loaded later
    */
    bool SaveDeck(std::string deck_name);
    bool LoadDeck(std::string deck_name);
    bool IsGameFile(std::string deck_name);
    std::vector<Card::card> GetHand(int id);

    void printDeck();
    card DrawCard(int id);
    void SetPlayer(card Set_card, int id);
    void SetDiscard(card Discard_card);
    /**
    * vector containing cards currently in deck
    */
    std::vector<s_card> n_deck;
    /**
    * vector containing cards that have been used.
    */
    //std::vector<Card> pile;
public:
    void CreatDeck();
    void ShuffleDeck();
    int GetInt();
    void PrintCard(card card2print);
};

#endif // DECK_H
