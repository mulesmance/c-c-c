#ifndef CARD_H
#define CARD_H

class Card
{
public:
    /**
    * constructor to create card object
    */

    typedef enum Card_suits {hearts, spades, clubs, diamonds};

    typedef struct card
    {
        Card_suits CardSuits;
        int faces;
        int begin;
        int push_back;
        int empty;
    };

    /**
    * returns value of card.
    */

    /**
    *returns face of card.
    */

private:
    int Value;
    char Suit;

};
#endif // CARD_H
