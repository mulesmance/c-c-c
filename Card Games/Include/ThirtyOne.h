#ifndef THIRTYONE_H
#define THIRTYONE_H
#include "Deck.h"

class ThirtyOne:public Deck{
public:
    ThirtyOne();
    /**
    * Plays the game thirty one. returns to menu once finished. saves throught play and can load previous games.
    */
    void Play_31();
};
#endif // THIRTYONE_H
