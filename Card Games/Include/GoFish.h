#ifndef GOFISH_H
#define GOFISH_H
#include "Deck.h"

using namespace std;

class GoFish:public Deck
{
    public:

        GoFish();
        void Play_GoFish();
        int CheckBook(std::vector<Card::card> ChkHand);
        std::vector<Card::card> RemoveBook(std::vector<Card::card> RemHand, int faceToRem);
        bool LoadPrvGame();
        void CreateHands();
        int GetInput();
        ~GoFish();
//        string L = "GoFish";
    private:
        void PrintHelp(int choice);
        void SetExit(int choice);
        Deck D;
        int load=0;
        int winner=0;
        int size1=0;
        int input=0;
        int BookFace = 0;
        int serch;
        card temp;
        card temp2;
        card temp3;
        bool exit = false;
        bool PlayerAction=false;
        bool CompAction=false;
        bool DeckEmpty=false;
        bool play=true;
        std::vector<Card::card> CompHand;
        std::vector<Card::card> PlayerHand;
        int comp_score = 0, plyr_score = 0;
        int comp_ask = 0;
};
#endif

/*
class GoFish:public Deck
{
public:
    GoFish();
    //begin game
    void play_gofish();

    void ask();

    void stealCard();

    void AddToSmallPile();

    void refill();


};
#endif // 21_H


*/
