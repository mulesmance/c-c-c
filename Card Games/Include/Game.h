#ifndef GAME_H
#define GAME_H

class Game
{
public:
    Game();
    /**
    *Takes in an int value that the user provides in main, and uses that value to select and run the appropriate game.
    */
    void Play(int i);
    int state;
};
#endif // GAME_H
