#ifndef CRAZY8_H
#define CRAZY8_H
#include "Deck.h"

class Crazy8:public Deck
{
    public:

    Crazy8();
    /**
    *Plays the game crazy 8. returns to menu once finished. saves throught play and can load previous games.
    */
    void Play_Crazy8();
};
#endif // CRAZY8_H
