#ifndef SNAP_H
#define SNAP_H
#include <Deck.h>

class snap:public Deck
{
public:
    //constructor
    snap();
    /** Runs the game of Snap. Once the game ends, the user will be asked to play again or to return
    to the main menu to either choose another game or to quit **/
    void play_snap();
};
#endif // 21_H

