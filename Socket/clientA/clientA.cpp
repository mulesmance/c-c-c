#include <sys/types.h>
#include <sys/socket.h> // socket structure/class
#include <string.h> // string type
#include <stdio.h> //cin & cout
#include <thread_db.h> // to implement multiple threads
#include <netinet/in.h>
#include <iostream>
#include <arpa/inet.h>
#include "messageHeader.h"

//definitions
#define SERVER "127.0.0.1" //IP address of udp server
#define BUFLEN 512  //Max length of buffer
#define PORT 8888   //The port on which to listen for incoming data


//standard namespace
using namespace std;

void connectClient();

//declarations
    int protocol = 17; // UDP protocol:17
    int s; // socket number
    char message[BUFLEN]; // the message to send; limited in size of buffer
    char buf[BUFLEN];// the buffer/message to be sent/read of size BUFLEN=512
    int intBuf[BUFLEN];
    struct sockaddr_in saddr;
    int slen = sizeof(saddr);


int main()
{
    //connect();


//create socket
//The socket() function creates an unbound socket in a communications domain, and returns a file descriptor that can be used in later function calls that operate on sockets. -1 if failure.
//AF_INET = "Internet address"
//SOCK_DGRAM = “Provides datagrams, which are connectionless-mode, unreliable messages of fixed maximum length.”
    if((s = socket(AF_INET, SOCK_DGRAM, protocol)) == -1)
        cout<<"Socket creation failure" << endl;
    cout <<"Socket created" << endl;

//set up address structure
//AF_INET = “Internet address”
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(PORT);
    inet_pton(AF_INET, SERVER, &(saddr.sin_addr));

//Clear buffer
//memset(intBuf, '\0', BUFLEN);
    memset(buf, '\0', BUFLEN);

//start communication
//Receive function requires socket number, pointer to buffer/message, size of buffer, flags,
//recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) saddr, socklen_t * slen);

//send a message
    //cin.getline(message, BUFLEN);
    //sendto(s, message, strlen(message), 0, (struct sockaddr *) &saddr, slen);
connectClient();


    return 0;



}

  void connectClient(){  //send a connection request to the server


    cin.getline(message, BUFLEN);
    sendto(s, message, strlen(message), 0, (struct sockaddr *) &saddr, slen);
    memset(buf, '\0', BUFLEN);
    recvfrom(s, intBuf, BUFLEN, 0, (struct sockaddr* ) &saddr, (socklen_t *) &slen);
    //recv(s, intBuf, BUFLEN, 0);
    cout<<endl<<intBuf<<endl;

    }
