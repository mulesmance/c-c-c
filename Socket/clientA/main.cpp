#include "sys/types.h"
#include "sys/socket.h"
#include "string.h"
#include "stdio.h"
#include "thread_db.h"
#include "netinet/in.h"
#include "iostream"
#include <arpa/inet.h>


#define SERVER "127.0.0.1"
#define BUFLEN 512
#define PORT 8888

using namespace std;

int main()
{

    int protocol = 17; //UDP protocol:17
    int s; //socket umber
    char message[BUFLEN]; //the message to send; limited in size of buffer
    char buf[BUFLEN];
    struct sockaddr_in saddr;
    int slen = sizeof(saddr);
    int error = -1;

//create socket
    socket(AF_INET, SOCK_DGRAM, protocol);
    if((s = socket(AF_INET, SOCK_DGRAM, protocol)) == error)
    cout<<"Socket creation failure" <<endl;
    cout<<"Socket created" << endl;

//set up address stucture
//AF_INET = “Internet address”
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(PORT);
    inet_pton(AF_INET, SERVER, &(saddr.sin_addr));


//Clear buffer
    memset(buf, '\0', BUFLEN);

//start communication
//Receive function requires socket number, pointer to buffer/message, size of buffer, flags,
    recvfrom(s, buf, BUFLEN, 0, (struct sockaddr* ) &saddr, (socklen_t *) &slen);


    return 0
