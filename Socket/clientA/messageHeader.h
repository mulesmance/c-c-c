#include <string.h>
#include <iostream>
#include <stdint.h>

using namespace std;

class message {
public:
message(short int a, string b, unsigned long int c, unsigned long int d, string e): seqNo(a), type(b), source(c), destination(d), payload(e){}
    short int seqNo;
    string type;
    unsigned long int source;
    unsigned long int destination;
    string payload;


};
//to store message using pointers
class serverMessage {
public:
serverMessage(message a, serverMessage * b, bool c): msg(a), next(b), last(c){}
    message msg;
    serverMessage * next;
    bool last;
};

//to maintain a list
class client {
public:
    client(unsigned long int a, short int b, bool c, client * d): destination(a), seqNo(b), last(c), next(d){}
    unsigned long int destination;
    short int seqNo;
    bool last;
    client * next;
};
