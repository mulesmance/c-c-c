#include <string.h>
#include <iostream>
#include <stdint.h>

#define maxl 512

using namespace std;

class message
{
public:
    message(short int a, short int b, unsigned long int c, unsigned long int d, char* e){
        seqNo=a;
        type=b;
        source=c;
        destination=d;
        //payload = e;
        copy(e,e+maxl,payload);
    }
    short int seqNo;
    short int type;//1 == get, 2 == send, 3 == ack;
    unsigned long int source;
    unsigned long int destination;
    char payload[maxl];
};
//to store message using pointers
class serverMessage
{
public:
    serverMessage(message * a, serverMessage * b, bool c): msg(a), next(b), last(c) {}
    message * msg;
    serverMessage * next;
    bool last;
};

//to maintain a list
class client
{
public:
    client(unsigned long int a, short int b, bool c, client * d): destination(a), seqNo(b), last(c), next(d) {}
    unsigned long int destination;
    short int seqNo;
    bool last;
    client * next;
};


