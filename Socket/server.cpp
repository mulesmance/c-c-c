#include "sys/types.h"
#include "sys/socket.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "messageHeader.h"
#include <signal.h>
#include "netinet/in.h"
#include "iostream"
#include <arpa/inet.h>
#define PORT 32566

using namespace std;

void addClient(unsigned long int);
void nowListen();
void addMessage(message*);
void removeMessages(unsigned long int);
void removeMessage(short int, unsigned long int, unsigned long int);
void sendAllMessages(unsigned long int);
void removeClient(unsigned long int);
void showAllClients();
void updateClients(int);
void requestServerConnection();
void connectMenu();
void acceptServerConnection(message*);
void forwardMessage(message*);

//global declarations
unsigned long int ID = 9999999995;
client * d;
client *clientList= new client(1111111111,1,1,d);
message tempMessage(1,1,123890,9999999994,"payload");
message * msgP = new message(1,1,123890,9999999994,"payload");
serverMessage * sm;
serverMessage * fsm = new serverMessage(msgP,sm, 1);
const int buflen = sizeof(tempMessage);
char buf[buflen];
int protocol = 17; //UDP protocol:17
int s; //socket umber
int ss;
char mess[buflen]; //the message to send; limited in size of buffer
sockaddr_in saddr, ssaddr;
//struct sockaddr_in ssaddr;
int sslen = sizeof(ssaddr);
int slen = sizeof(saddr);
int error = -1;
int serverNo;
char SERVERIP[maxl];
char firstSERVERIP[maxl];
bool first;
bool last;


int main()
{
    last = 1;
    serverNo = 1;
    first = 1;
    cout<<"Enter this server's IP: "<<flush;
    cin.getline(firstSERVERIP,100);
//create socket
    if((s = socket(AF_INET, SOCK_DGRAM, protocol)) == error)
        cout<<"Socket creation failure" <<endl;
    cout<<"Socket created" << endl;

//set up address structure
//AF_INET = “Internet address”
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(PORT);
    saddr.sin_addr.s_addr = INADDR_ANY;

//bind socket
    bind(s, (struct sockaddr *)&saddr, sizeof(saddr));



    if((ss = socket(AF_INET, SOCK_DGRAM, protocol)) == -1)
        cout<<"Socket creation failure" << endl;
    cout <<"Socket created" << endl;
    ssaddr.sin_family = AF_INET;
    ssaddr.sin_port = htons(PORT);

    pid_t f;
    f = fork();
    if(f != 0){
        requestServerConnection();
        kill(f, SIGKILL);
    }

    while(true)
    {
        nowListen();
    }

    return 0;
}

void nowListen()
{
    message * h = new message(1, 1, 1, 1, "");
    memset(h, '\0', sizeof(*h));
    recvfrom(s, h, buflen, 0, (struct sockaddr* ) &saddr, (socklen_t *) &slen);
    if(h->type == 0)
    {
        addClient(h->source);
    }
    if(h->type == 3)
    {
        removeMessage(h->seqNo,h->source, h->destination);
    }
    if(h->type == 2)
    {
        forwardMessage(h);
    }
    if(h->type==1)
    {
        sendAllMessages(h->source);
    }
    if(h->type == 4)
    {
        removeClient(h->source);
        //showAllClients();
    }
    if(h->type==6)
    {
        acceptServerConnection(h);
    }
}

void removeMessage(short int seqNo, unsigned long int source, unsigned long int dest)
{
    serverMessage* smp = fsm;
    serverMessage* prev = fsm;
    do
    {
        if(smp!=fsm)
        {
            prev = smp;
        }
        smp = smp->next;
        if(smp->msg->seqNo == seqNo && smp->msg->destination == source && smp->msg->source == dest)
        {
            if(smp->last!=1)
            {
                prev->next = smp->next;
            }
            else
            {
                prev->last=1;
                prev->next=fsm;
            }
        }
    }
    while(smp->last!=1);
}

void sendAllMessages(unsigned long int destination)
{
    serverMessage* y =fsm;
    do
    {
        if(y->last!=1)
        {
            y = y->next;
        }
        if(y->msg->destination==destination)
        {
            sendto(s, y->msg, buflen, 0, (struct sockaddr *) &saddr, slen);
            //delete ACK
            if(y->msg->type==3)
            {
                removeMessage(y->msg->seqNo, y->msg->destination, y->msg->source);
            }
        }
    }
    while(y->last!=1);
    message * m = new message(1,1,9999999997,destination,"");
    sendto(s, m, buflen, 0, (struct sockaddr *) &saddr, slen);
    // removeMessages(destination);
}

void addMessage(message * m)
{
    serverMessage* smp = fsm;
    while(smp->last!=1)
    {
        smp = smp->next;
    }
    smp->last=0;
    serverMessage* smp1 = new serverMessage(m,fsm,1);
    smp->next = smp1;
}

void addClient(unsigned long int destination)
{
    client *c = clientList;
    if(clientList->destination == 1111111111)
    {
        clientList->destination = 111111111 + (serverNo * 1000000000);
    }
    while(c->last!=1)
    {
        c= c->next;
        if(c->destination == destination)
        {
            return;
        }
    }
    c->last=0;
    c->next = new client(c->destination+1, 0, 1, clientList);
    message* messageClass = new message(1,3,ID, c->destination+1, "");
    sendto(s, messageClass, buflen, 0, (struct sockaddr *) &saddr, slen);
}

void removeClient(unsigned long int destination)
{
    client * c = clientList;
    client * prev = clientList;
    do
    {
        if(c!=clientList)
        {
            prev=c;
        }
        c=c->next;
        if(c->destination == destination)
        {
            if(c->last!=1)
            {
                prev->next=c->next;
            }
            else
            {
                prev->last=1;
                prev->next=clientList;
            }
        }
    }
    while(c->last!=1);
}

void showAllClients()
{
    client * c = clientList;
    cout<<"Client list:"<<endl;
    do
    {
        if(c->last!=1)
        {
            c=c->next;
        }
        cout<<c->destination<<endl;
    }
    while(c->last!=1);
}

void updateClients(int prevServerNo)
{
    int newServerNo = prevServerNo + 1;
    client * c = clientList;
    message * m;
    unsigned long int newID;
    do
    {

        if(c->destination<(newServerNo * 1000000000))
        {
            newID = c->destination - (serverNo * 1000000000) + (newServerNo * 1000000000);
            m = new message(1,5,newID,c->destination,"ID update!");
            addMessage(m);
            addClient(newID);
            removeClient(c->destination);
        }
        if(c->last!=1)
        {
            c=c->next;
        }
    }
    while(c->last!=1);
    serverNo = newServerNo;
}

void connectMenu()
{
    char i;
    do
    {
        cout<<"Enter server IP address: "<< endl;
        cin.getline(SERVERIP,maxl);
        cout<<"Enter 'c' to connect, enter any other key to re-enter server IP."<<endl;
        cin>>i;
    }
    while(i!='c');
    cin.ignore();
}

void requestServerConnection()
{
    connectMenu();
    inet_pton(AF_INET, (const char*)SERVERIP, &(ssaddr.sin_addr));
    message * m = new message(1,6,serverNo,9999999999,firstSERVERIP);
    sendto(ss,m, buflen, 0, (struct sockaddr *) &ssaddr, sslen);
    memset(m, '\0', buflen);
    recvfrom(ss, m, buflen, 0, (struct sockaddr* ) &ssaddr, (socklen_t *) &sslen);
    if(m->seqNo == 1)
    {
        cout <<"Request accepted." << endl;
        last = 0;
    }
    else
    {
        cout <<"Request denied." << endl;
        requestServerConnection();
    }
}

void acceptServerConnection(message* h)
{
    message * m;
    if(first == 1)
    {
        m = new message(1,6,serverNo,h->source,"");
       sendto(s,m, buflen, 0, (struct sockaddr *) &saddr, slen);
        updateClients(h->source);
        copy(h->payload,h->payload+maxl,firstSERVERIP);
        first = 0;
        if(last == 1)
        {
            inet_pton(AF_INET, (const char*)firstSERVERIP, &(ssaddr.sin_addr));
        }
    }
    else
    {
        m = new message(0,6,serverNo,h->source,"");
        sendto(s,m, buflen, 0, (struct sockaddr *) &saddr, slen);
    }
}

void forwardMessage(message* w)
{

    serverMessage* p = fsm;
    do{
        if(p->msg->seqNo == w->seqNo && p->msg->source == w->source && p->msg->destination == w->destination)
        {
            return;
        }
        if(p->last!=1)
        {
            p=p->next;
        }
    }while(p->last != 1);
    if(p->msg->seqNo == w->seqNo && p->msg->source == w->source && p->msg->destination == w->destination)
    {
        return;
    }

    addMessage(w);
    if(first != 1 || last != 1)
    {
        sendto(ss,w, buflen, 0, (struct sockaddr *) &ssaddr, sslen);
    }
}
