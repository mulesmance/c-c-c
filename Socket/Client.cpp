#include <sys/types.h>
#include <sys/socket.h> // socket structure/class
#include <string.h> // string type
#include <stdio.h> //cin & cout
#include <thread_db.h> // to implement multiple threads
#include <netinet/in.h>
#include <iostream>
#include <arpa/inet.h>
#include <unistd.h>
#include "messageHeader.h"
#include <signal.h>

//definitions
#define SERVER "127.0.0.1" //IP address of udp server
#define PORT 32566   //The port on which to listen for incoming data


//standard namespace
using namespace std;

void sendMessage();
short int getSeqNo(unsigned long int);
void connectClient();
void addClient(unsigned long int);
void receive();
void getMessage();
void generateACK(short int, unsigned long int);
void menu();
void leave();
void connectMenu();

//declarations
const int buflen = sizeof(message);
const int protocol = 17; // UDP protocol:17
int s; // socket number
char mess[buflen]; // the message to send; limited in size of buffer
char buf[buflen];// the buffer/message to be sent/read of size BUFLEN=512
struct sockaddr_in saddr;
int slen = sizeof(saddr);
unsigned long int ID = 1111111111;
client * d;
client *clientList= new client(1111111111,1,1,d);
char SERVERIP[100];

int main()
{
//create socket
//SOCK_DGRAM = “Provides datagrams, which are connectionless-mode, unreliable messages of fixed maximum length.”
    if((s = socket(AF_INET, SOCK_DGRAM, protocol)) == -1)
        cout<<"Socket creation failure" << endl;
    cout <<"Socket created" << endl;

//set up address structure
//AF_INET = “Internet address”
    connectMenu();
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(PORT);
    inet_pton(AF_INET, (const char*)SERVERIP, &(saddr.sin_addr));

    connectClient();
    //Clear buffer
    //memset(buf, '\0', buflen);
    menu();

    return 0;
}



short int getSeqNo(unsigned long int destination){
    client* s = clientList;
    do{
        if(destination == s->destination){
            return s->seqNo;
        }
        s = s->next;
    }while(1 != s->last);
    if(destination == s->destination){
            return s->seqNo;
        }

        else return -1;
}

void getMessage(){
    message * m = new message(0,1,ID,ID,"");
    sendto(s,( const char*)m, buflen , 0, (struct sockaddr *) &saddr, slen);
    receive();
}

void generateACK(short int seqNo, unsigned long int dest){
    message* ack = new message(seqNo,3,ID,dest,"");
    sendto(s, ack, buflen , 0, (struct sockaddr *) &saddr, slen);
    }

void receive(){
    pid_t childPID;
    message * m = new message(0,1,ID,9999999998,"");
    memset(m,'\0',sizeof(*m));
    recvfrom(s, m, buflen, 0, (struct sockaddr* ) &saddr, (socklen_t *) &slen);
    if(m->type != 1){
        childPID = fork();
        if(childPID != 0){
                receive();
                kill(childPID, SIGKILL);
        }
        if(m->type == 2){
            cout<<m->source<<": "<<m->payload<<endl;
            generateACK(m->seqNo,m->source);
        }
        if(m->type == 5)
        {
            cout<<m->source<<": "<<m->payload<<endl;
            ID=m->source;
            generateACK(m->seqNo,m->source);
        }

    }
    return;//end forked thread
}

void sendMessage(){
    short int type = 2;
    char payload[maxl];
    unsigned long int destination;

    cout<<"Enter recipient ID: "<<endl;
    cin>>destination;
    addClient(destination);
    cin.ignore();

    cout<<"Enter message: "<< endl;
    cin.getline(payload,maxl);
    message * m = new message(getSeqNo(destination), type, ID, destination, payload);
    client* c = clientList;
    do{
      if(destination == c->destination){
         c->seqNo++;
      }
      c = c->next;
    }while(c->last!=1);
    if(destination == c->destination){
         c->seqNo++;
      }
    sendto(s,( const char*)m, buflen , 0, (struct sockaddr *) &saddr, slen);
}

void connectClient(){  //send a connection request to the server
    message* messageClass = new message(1,0,ID,9999999996,"payload");
    sendto(s,( const char*)messageClass, buflen , 0, (struct sockaddr *) &saddr, slen);
    memset(buf, '\0', buflen);
    memset(messageClass,'\0',buflen);
    recvfrom(s, messageClass, buflen, 0, (struct sockaddr* ) &saddr, (socklen_t *) &slen);
    ID = messageClass->destination;
    cout << "Your ID number is: " << ID << endl;

}

void addClient(unsigned long int destination){
    client* c = clientList;
    while(c->last!=1)
    {
        if(c->destination == destination)//have we already added this client?
        {
            return;//duplicate client - do not add
        }
        c= c->next;
    }
    if(c->destination == destination)//have we already added this client?
    {
        return;//duplicate client - do not add
    }
    c->last=0;
    c->next = new client(destination, 0, 1, clientList);
}

void menu()
{
    char i;

    i='c';
    do{
        cout<<"Enter 'g' to send a GET request, enter 's' to compose a new message, enter 'q' to disconnect and quit."<<endl;
        cin>>i;
        if(i=='g')
        {
            getMessage();
        }
        if(i=='s')
        {
            getMessage();
            sendMessage();
        }
    }while(i!='q');
    cin.ignore();
    leave();
    cout<<"You have disconnected. Quitting..." <<endl;
}

void leave()
{
    message * m = new message(1,4,ID,9999999999,"");
    sendto(s, m, buflen , 0, (struct sockaddr *) &saddr, slen);
}

void connectMenu()
{
    char i;
    do{

        cout<<"Enter server IP address: "<<flush;
        cin.getline(SERVERIP,100);
        cout<<"Enter 'c' to connect, enter any other key to re-enter server IP."<<endl;
        cin>>i;
    }while(i!='c');
    cin.ignore();
}
