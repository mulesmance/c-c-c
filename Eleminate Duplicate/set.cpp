#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

template <class S>

class setTemp {
  vector<S>set;
    
public:
  setTemp(){}
  setTemp(const setTemp& x): set(x.set){}
  ~setTemp()
  {
    set.clear();
  }

  void add(S s)
  {
    set.push_back(s);
  }

  //nested iterator class that support the end "end sentinel" concept
  class iterate {
  setTemp<S>x;
  int index;
      
  public:
    iterate(setTemp<S> xx): x(xx), index(0){}
    //"end sentinel" operator
    iterate(setTemp<S> xx, bool) :x(xx){}

    S operator*()
    {
      return x.set.at(index);
    }
    
    S operator++()
    {
      return ++x.set.at(index);
    }
    
    S operator++(int)
    {
      return x.set.at(index)++;
    }
    
    bool operator!=(const iterate& it)const
    {
      return index != it.index;
    }
  };

  iterate begin()
  {
    return iterate (*this);
  }
  
  iterate end()
  {
    return iterate (*this, true);
  }
};
